import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    loginIn: false,
    loginError: null,
    loginSuccessful: false
  },
  mutations: {
    loginStart: state => state.loginIn = true,
    loginStop: (state, errorMessage) => {
      state.loginIn = false;
      state.loginError = errorMessage;
      state.loginSuccessful = !errorMessage;
    }
  },
  actions: {
    doLogin({commit}, loginData) {
      commit('loginStart');

      axios.post('http://192.168.1.225:3333/api/usuarios', {
        ...loginData
      })
      .then(()=>{commit('loginStop', null)
      })
      .catch(error => {
        commit('loginStop', error.response.data.error)
      })
    }
  }
})
